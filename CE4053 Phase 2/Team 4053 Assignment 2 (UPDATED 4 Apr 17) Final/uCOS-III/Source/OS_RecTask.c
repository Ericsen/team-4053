#include <OS_RecTask.h>

/* root node */
temp_node *roots = (temp_node*)0;

//TEMP TCB
OS_MEM          OS_Temp_MEM;
Temp_TCB        OS_Temp_TCB_STORAGE[40];

//AVL
OS_MEM          OS_TCB_MEM;
OS_TCB          OS_TCB_STORAGE[50];

//AVL memory block
OS_MEM          AVL_MEM;
node            AVL_STORAGE[50];

//Binary Heap
OS_MEM			HeapNodeMEM;
HeapNode   		alloMemHeapNode[20];

//Heap Tree
OS_MEM			HeapTreeMem;
HeapTree		alloMemHeapTree[1];

//Heap Tree Reference
HeapTree *heapTree = (HeapTree *)0;
HeapTree *heapTreeTemp = (HeapTree *)0;

//Red Black Tree Node
OS_MEM            redblacktreeNodeMEM;
RedBlackNode      alloMemredblackNode[40];   //1 row for 40 words of RedBlackNode

//Red Black Tree
OS_MEM            redblackTreeMEM;
RedBlackTree      alloMemredblackTree[1];    //1 row for 1 word of RedBlackTree

//Red Black recursive List reference
RedBlackTree *redBlackTree = (RedBlackTree *)0;

//Stack variable
const int STACK_MAXSIZE = 20;        
int top = -1; 

//Stack
OS_MEM            Stack_MEM;
OS_MUTEX*         Stack_STORAGE[20];

//System Ceiling
int sys_ceil = 30;

//Temp TCB variable
Temp_TCB* temps_tcb;

//No. of element
int element = 0;
//Counter Variable
int counter = 0;

int start_time = 0;
int start_time2 = 0;
int time = 0;
int time1 = 0;
int overall_time = 0;

int first = 0;
int second = 0;

/*
********************************************************************************************
                               Create Memory Partition 
********************************************************************************************
*/
void AVLTree_init(void)
{
      OS_ERR        err;
      
      OSMemCreate(  (OS_MEM         *)&AVL_MEM,
                    (CPU_CHAR       *)"AVLPartition",
                    (void           *)&AVL_STORAGE[0],
                    (OS_MEM_QTY      ) 50,
                    (OS_MEM_SIZE     ) sizeof(node),
                    (OS_ERR         *)&err);
      
      OSMemCreate(  (OS_MEM         *)&OS_TCB_MEM,
                    (CPU_CHAR       *)"OS_TCBPartition",
                    (void           *)&OS_TCB_STORAGE[0],
                    (OS_MEM_QTY      ) 50,
                    (OS_MEM_SIZE     ) sizeof(OS_TCB),
                    (OS_ERR         *)&err);
      
      OSMemCreate(  (OS_MEM         *)&OS_Temp_MEM,
                    (CPU_CHAR       *)"OS_TempPartition",
                    (void           *)&OS_Temp_TCB_STORAGE[0],
                    (OS_MEM_QTY      ) 50,
                    (OS_MEM_SIZE     ) sizeof(Temp_TCB),
                    (OS_ERR         *)&err);
}

void BinaryHeap_init(void)
{	
	OS_ERR err;
	
	//printf ("Initialize Binary Heap\n");
	
	//Allocate Memory to Binary Heap Node
	OSMemCreate(
		(OS_MEM		*) &HeapNodeMEM,
		(CPU_CHAR	*) "Heap Node Partition",
		(void		*) &alloMemHeapNode,
		(OS_MEM_QTY  ) 20,
		(OS_MEM_SIZE ) sizeof(HeapNode),
		(OS_ERR		*) &err
	);
	
	//allocate memory to binary heap tree
	OSMemCreate(
		(OS_MEM		*) &HeapTreeMem,
		(CPU_CHAR	*) "Heap Tree Partition",
		(void		*) &alloMemHeapTree,
		(OS_MEM_QTY  ) 1,
		(OS_MEM_SIZE ) sizeof(HeapTree),
		(OS_ERR		*) &err
	);
	
	//initialize the node
	heapTree = (HeapTree*) OSMemGet((OS_MEM	*)&HeapTreeMem,
					(OS_ERR *)&err);
	
	heapTree->root = (HeapNode*)0;
	heapTree->data = &alloMemHeapNode[0];
	
	int i = 0;
	while (i < 20)
	{
		alloMemHeapNode[i].ptr_tcb = (OS_TCB*)0;
		i++;
	}
	
	heapTree->count = (CPU_INT08U) 0;
	heapTree->size = (CPU_INT08U )20;
}

void RedBlackTree_init(void)
{
    //Allocate Memory for Binary Nodes
    
    OS_ERR err;
     
    //Allocate Memory to Red Black Tree Node
    OSMemCreate((OS_MEM         *)&redblacktreeNodeMEM,
                (CPU_CHAR       *)"RedBlackTreeNodePartition",
                (void           *)&alloMemredblackNode[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(RedBlackNode),
                (OS_ERR         *)&err);
    
    
    //Allocate Memory to Red Black Tree
    OSMemCreate((OS_MEM         *)&redblackTreeMEM,
                (CPU_CHAR       *)"RedBlackTreePartition",
                (void           *)&alloMemredblackTree[0],
                (OS_MEM_QTY      ) 2,
                (OS_MEM_SIZE     ) sizeof(RedBlackTree),
                (OS_ERR         *)&err);
    
     //Allocate Memory to Temp_TCB
    OSMemCreate((OS_MEM         *)&OS_Temp_MEM,
                (CPU_CHAR       *)"Temp_TCB",
                (void           *)&OS_Temp_TCB_STORAGE[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(Temp_TCB),
                (OS_ERR         *)&err);    
    
    //Initialize nodelists
    redBlackTree = (RedBlackTree*)OSMemGet(
                                      (OS_MEM  *)&redblackTreeMEM, 
                                      (OS_ERR  *)&err);
    
    redBlackTree->rootNode = (RedBlackNode*)0;
    redBlackTree->neel =  (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);
    redBlackTree->neel->color = 1       ;
}
                
/*
*****************************************************************************************************
                                   AVL TREE & FUNCTION PROTOTYPE
*****************************************************************************************************
*/
//Update time function calls the recursive update time
void AVLTree_Update_Time()
{
        temp_node *temp = roots;
        AVLTree_Recursive_Update(temp);
}

/*Recursively check the counter value with fixed timing, if match 
    it will be inserted into binary heap.*/
void AVLTree_Recursive_Update(temp_node* curNode)
{       
        OS_ERR err;
        
        //If curNode = null, return
        if(curNode == (temp_node*)0)
            return;
        
        OSSchedLock(&err);
        
        if(counter != 0)
        {
          if(counter%5 == 0) //Period = 5
          {
                Insert_To_Binary_Heap(_search(curNode,1u));
                Insert_To_Binary_Heap(_search(curNode,2u));
          }
          if(counter%10 == 0) //Period = 10
                Insert_To_Binary_Heap(_search(curNode,3u));
        }
        
        //Increment Counter
        counter++;
        
        OSSchedUnlock(&err);
}

void Insert_To_Binary_Heap(temp_node* curNode)
{
    OS_ERR err;
    Temp_TCB *tempo_tcb = (Temp_TCB*)0;
    tempo_tcb = curNode->data;
              
    OS_RecTaskBinaryHeapCreate(
         (OS_TCB        *) tempo_tcb->p_tcb,
         (CPU_CHAR      *) tempo_tcb->p_name, 
         (OS_TASK_PTR    ) tempo_tcb->p_task, 
         (void          *) tempo_tcb->p_arg, 
         (OS_PRIO        ) tempo_tcb->prio, 
         (CPU_STK       *) tempo_tcb->p_stk_base, 
         (CPU_STK_SIZE   ) tempo_tcb->stk_limit, 
         (CPU_STK_SIZE   ) tempo_tcb->stk_size, 
         (OS_MSG_QTY     ) tempo_tcb->q_size, 
         (OS_TICK        ) tempo_tcb->time_quanta, 
         (void          *) (CPU_INT32U) tempo_tcb->p_ext, 
         (OS_OPT         )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
         (OS_ERR        *) &err,
         (CPU_INT32U     ) tempo_tcb->period);
              
   Heap_Insert(tempo_tcb->p_tcb);
}

temp_node * _search(temp_node* T, int key)
{
	if (T == 0)
		return 0;

	if (key < T->data->period)
		return _search(T->left, key);
	else if (key > T->data->period)
		return _search(T->right, key);
	else
		return T;
}

temp_node *_insert(temp_node* T, Temp_TCB *x)
{
        OS_ERR      err;
        
	if (T == (temp_node*)0)
	{
		T = (temp_node*)OSMemGet((OS_MEM *)&AVL_MEM,
                                     (OS_ERR *)&err);
    
                T->data = (Temp_TCB*)OSMemGet((OS_MEM *)&OS_TCB_MEM,
                                         (OS_ERR *)&err);
		T->data = x;
		T->left = (temp_node*)0;
		T->right = (temp_node*)0;
	}
	else
	if (x->period > T->data->period)        /* insert in right subtree*/
	{
		T->right = _insert(T->right, x);
		if (_BF(T) == -2)
		if (x->period >T->right->data->period)
			T = _RR(T);
		else
			T = _RL(T);
	}
	else
	if (x->period < T->data->period)		/*insert in left subtree*/
	{
		T->left = _insert(T->left, x);
		if (_BF(T) == 2)
		if (x->period < T->left->data->period)
			T = _LL(T);
		else
			T = _LR(T);
	}

	T->ht = _height(T);
        
        return(T);
}

temp_node *_Delete(temp_node* T, int x)
{
	temp_node *p;

	if (T == 0)
	{
		return 0;
	}
	else if (x > T->data->period)        
	{
		T->right = _Delete(T->right, x);
		if (_BF(T) == 2)
		if (_BF(T->left) >= 0)
			T = _LL(T);
		else
			T = _LR(T);
	}
	else if (x < T->data->period)
	{
		T->left = _Delete(T->left, x);
		if (_BF(T) == -2)    /*Rebalance during windup*/
		if (_BF(T->right) <= 0)
			T = _RR(T);
		else
			T = _RL(T);
	}
	else
	{
		/*data to be deleted is found*/
		if (T->right != 0)
		{    /*delete its inorder succesor*/
			p = T->right;

			while (p->left != 0)
				p = p->left;

			T->data = p->data;
			T->right = _Delete(T->right, p->data->period);

			if (_BF(T) == 2)/*Rebalance during windup*/
			if (_BF(T->left) >= 0)
				T = _LL(T);
			else
				T = _LR(T);
		}
		else
			return(T->left);
	}
	T->ht = _height(T);
	return(T);
}

//Return the height of the tree
int _height(temp_node * T)
{
	int lh, rh;
	if (T == 0)
		return(0);

	if (T->left == 0)
		lh = 0;
	else
		lh = 1 + T->left->ht;

	if (T->right == 0)
		rh = 0;
	else
		rh = 1 + T->right->ht;

	if (lh>rh)
		return(lh);

	return(rh);
}

//Rotate right once
temp_node *_rotateright(temp_node * x)
{
	temp_node *y;
	y = x->left;
	x->left = y->right;
	y->right = x;
	x->ht = _height(x);
	y->ht = _height(y);
	return(y);
}

//Rotate left once
temp_node *_rotateleft(temp_node * x)
{
	temp_node *y;
	y = x->right;
	x->right = y->left;
	y->left = x;
	x->ht = _height(x);
	y->ht = _height(y);

	return(y);
}

temp_node * _RR(temp_node * T) /*Right-Right case*/
{
	T = _rotateleft(T);
	return(T);
}

temp_node * _LL(temp_node * T) /*Left-Left case*/
{
	T = _rotateright(T);
	return(T);
}

temp_node * _LR(temp_node * T) /*Left-Right case*/
{
	T->left = _rotateleft(T->left);
	T = _rotateright(T);

	return(T);
}

temp_node * _RL(temp_node * T) /*Right-Left case*/
{
	T->right = _rotateright(T->right);
	T = _rotateleft(T);
	return(T);
}

//Return the balancing factor of the node
int _BF(temp_node * T) /*Balance Factor = height of left sub-tree - height of right sub-tree*/
{
	int lh, rh; /*Left tree height and right tree height*/
	if (T == 0)
		return(0);

	if (T->left == 0) /*If the root got no left node, left tree height = 0*/
		lh = 0;
	else
		lh = 1 + T->left->ht;

	if (T->right == 0) /*If the root got no right node, right tree height = 0*/
		rh = 0;
	else
		rh = 1 + T->right->ht;

	return(lh - rh); /*Return the balance factor*/
}

void _inorder(temp_node *T) /*in-order traversal*/
{
	if (T != 0)
	{
		_inorder(T->left);
		printf("%d(Bf=%d)", T->data->period, _BF(T));
		_inorder(T->right);
	}
}

void print(temp_node *T)
{
	printf("\n\nInorder sequence:\n");
	inorder(T);
	printf("\n");
	printf("\nHeight: %d",height(roots));
	printf("\n");
        printf("\n");
}

/*
**********************************************************************************************************************************
                                            BINARY HEAP & FUNCTION PROTOTYPE
**********************************************************************************************************************************
*/
// Inserts element to the heap
void Heap_Insert(OS_TCB* _ptr_tcb)
{
      //start_time = OS_TS_GET();
  
      //get total number of the element in the list
      CPU_INT08U count = heapTree->count;
              
      //add the TCB into the data structure
      alloMemHeapNode[count].ptr_tcb = _ptr_tcb;
              
      //update the number of element
      count++;
              
      heapTree->count = count;
      //BinaryHeap_printTree();
      
      /*start_time2 = OS_TS_GET();
      insertion_time = start_time2 - start_time;
      insertion_time = insertion_time/50;
      overall_time = overall_time + insertion_time;
      printf("Insertion Time: %d us\n",insertion_time);*/
}

//Reordering the task according to the priority
void Heap_Heapify(void)
{
      //start_time = OS_TS_GET();
  
      //Gets current number of tasks
      CPU_INT08U count = heapTree->count;
      CPU_INT08U i = 0;
      HeapNode *lists  = &alloMemHeapNode[0];
      while(i < count)
      {
        Heap_heapify(i,lists);
        i++;
      }
      
      /*start_time2 = OS_TS_GET();
      heapifying_time = start_time2 - start_time;
      heapifying_time = heapifying_time/50;
      overall_time = overall_time + heapifying_time;
      printf("Heapifying Time: %d us\n",heapifying_time);*/
}

//PrintTree
void Heap_printTree(void)
{
      //Gets current number of tasks
      CPU_INT08U count = heapTree->count;
      CPU_INT08U i = 0;
      HeapNode *lists = heapTree->data;
      while(i<count)
      {
        printf("%d\n",lists[i].ptr_tcb->period);
        i++;
      }  
}

//Delete particular Nodes
void Heap_delete(OS_TCB *ptr_tcb)
{       
      //Get readyList
      HeapNode *lists = heapTree->data;
      //Search index of deleted Node
      CPU_INT08U deletedNode = Heap_search(ptr_tcb);
      //Gets current number of tasks
      CPU_INT08U count = heapTree->count;  
      
      if(deletedNode!= (CPU_INT08U)255)
      {
        Heap_swap(deletedNode,count-1);
      }
      
      lists[count-1].ptr_tcb = (OS_TCB*)0;
      Heap_Heapify();
      count--;
      heapTree->count = count;
}

//Pop the minimum node and heapify
OS_TCB* Heap_pop()
{
      OS_ERR err;
      //start_time = OS_TS_GET();
      
      //Get readyList
      HeapNode *lists = heapTree->data;
      OS_TCB* toBeReturned = lists[0].ptr_tcb;
      if(toBeReturned != 0)
      {
        Heap_delete(toBeReturned);
        
        OSMemPut( (OS_MEM *)&HeapTreeMem,
                          (void   *)lists,
                          (OS_ERR *) &err);
        
        /*start_time2 = OS_TS_GET();
        pop_min_time = start_time2 - start_time;
        pop_min_time = pop_min_time/50;
        overall_time = overall_time + pop_min_time;
        printf("Pop_min Time: %d us\n",pop_min_time);
        printf("Overall Time: %d us\n\n",overall_time);*/
        
        return toBeReturned;
      }
      else
      {
        return (OS_TCB*)0;
      }
}

//Return index of Nodes 
CPU_INT08U Heap_search(OS_TCB *ptr_tcb)
{
      //Gets current number of tasks
      CPU_INT08U count= heapTree->count;
      CPU_INT08U i = 0;
      HeapNode *lists = heapTree->data;
      while(i<count)
      {
        if(lists[i].ptr_tcb->period == ptr_tcb->period)
        {
          return i;
        }
        i++;
      }    
      return 255;
}

//Return LeftNodeIndex
CPU_INT08U Heap_leftNodeIndex(CPU_INT08U i)
{
    return i * 2 + 1;
}
  
//Return RightNodeIndex
CPU_INT08U Heap_rightNodeIndex(CPU_INT08U i)
{
    return i * 2 + 2;           
}

//Recursiviliy Heapify
void Heap_heapify(CPU_INT08U i,HeapNode *lists)
{
   if(i > 20)
      return;
   
   CPU_INT08U parent_Index = i;
   CPU_INT08U left_Index = Heap_leftNodeIndex(i);
   CPU_INT08U right_Index = Heap_rightNodeIndex(i);
   
   //if left node is valid
    if(left_Index < 20 && lists[left_Index].ptr_tcb != (OS_TCB *)0)
   {
     //Coniture go to the leftmost
     Heap_heapify(left_Index,lists);
     
     //If right is valid
      if(right_Index < 20 && lists[right_Index].ptr_tcb != (OS_TCB *)0)
     {
         if(lists[left_Index].ptr_tcb->period < lists[right_Index].ptr_tcb->period)
        {
           if(lists[left_Index].ptr_tcb->period < lists[parent_Index].ptr_tcb->period)
          {
            Heap_swap(left_Index,parent_Index);
          }
        }
        else
        {
           if(lists[right_Index].ptr_tcb->period < lists[parent_Index].ptr_tcb->period)
          {
            Heap_swap(right_Index,parent_Index);
          }         
        }
     }
     else
     {
       return;
     }
   }
   
   if(right_Index < 20)
   {
     Heap_heapify(right_Index,lists);
   }
}

//Swap the contents
void Heap_swap(CPU_INT08U i1,CPU_INT08U i2)
{
   OS_TCB *temp =  heapTree->data[i1].ptr_tcb;
   heapTree->data[i1].ptr_tcb =  heapTree->data[i2].ptr_tcb;
   heapTree->data[i2].ptr_tcb = temp;
}

/*
**********************************************************************************************************************************
                                      RED BLACK TREE & FUNCTION PROTOTYPE
**********************************************************************************************************************************
*/

//BST insertion
void RedBlackTree_insert(OS_TCB * _ptr_tcb)
{
  OS_ERR err;
  
  RedBlackNode *y = redBlackTree->neel;
  RedBlackNode *x = redBlackTree->rootNode;
  RedBlackNode *newNode = (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);
  
  newNode->ptr_tcb = _ptr_tcb;
  
  if(redBlackTree->rootNode != (RedBlackNode*)0)
  {
    while(x != redBlackTree->neel)
    {
      y = x;
      if(newNode->ptr_tcb->period < x->ptr_tcb->period)
      {
        x = x->left;
      }
      else
      {
        x = x->right;      
      }
    }
  }
  newNode->parent = y;
  
  if(y == redBlackTree->neel)
  {
    redBlackTree->rootNode = newNode;    
  }
  else if(newNode->ptr_tcb->period < y->ptr_tcb->period)
  {
    y->left = newNode;    
  }
  else
  {
    y->right = newNode;
  }
  
  newNode->left = redBlackTree->neel;
  newNode->right = redBlackTree->neel;
  newNode->color = (CPU_INT08U)0;
    
  RedBlackTree_InsertFix(newNode);
} 

//Red Black Tree deletion
void RedBlackTree_delete(OS_TCB *_ptr_tcb)
{
  OS_ERR err;
  
  //Check if the node is exist
  RedBlackNode *delete_node = search(_ptr_tcb);
  
  if(delete_node == (RedBlackNode*)0)
  {
    return;
  }
  
  //To be deleted and return memory 
  RedBlackNode *y = (RedBlackNode*)0;
  //The child to be deleted
  RedBlackNode *x = (RedBlackNode*)0;

  if(delete_node->left == redBlackTree->neel || delete_node->right == redBlackTree->neel)
  {
    y = delete_node;
  }
  else
  {
    y = Successor(delete_node);
  }
  
  //Reordering
  if(y->left != redBlackTree->neel)
  {
    x = y->left;
  }
  else
  {
    x = y->right;   
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;    
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
   y->parent->right = x;     
  }
  
  if(y != delete_node)
  {
    delete_node->ptr_tcb = y->ptr_tcb;
  }
  
  if(y->color ==1)
  {
    RedBlackTree_DeleteFix(x);
  }
  
  //Returning memory 
  
  OSMemPut((OS_MEM *)&redblacktreeNodeMEM,
           (void   *)y,
           (OS_ERR *) &err);
}              

//Additional function
RedBlackNode* LeftMost(RedBlackNode* current)
{
  while(current->left != (RedBlackNode*)0)
  {
    current = current->left; 
  }
  return current;
}

RedBlackNode* Successor(RedBlackNode* current)
{
  if(current->right != (RedBlackNode*)0)
  {
    return LeftMost(current->parent);   
  }
  
  RedBlackNode *newNode = current->parent;
  
  while(newNode != (RedBlackNode*)0 && current == newNode->right)
  {
    current = newNode;
    newNode = newNode->parent;    
  }
  return newNode;
}

//Arrange tree after insertion
void RedBlackTree_InsertFix(RedBlackNode *current)
{
  //case 0: if parent is black, no while loop
  
  while(current->parent->color == 0) //if parent is red, go into loop
  {
    //if parent is grandparent's left child
    
    if(current->parent == current->parent->parent->left)
    {
      RedBlackNode *uncle = current->parent->parent->right;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->right)
        {
          current = current->parent;
          RedBlackTree_LeftRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_RightRotate(current->parent->parent);
      }
    }
    //if parent is grandparent's right child
    else
    {
      RedBlackNode *uncle = current->parent->parent->left;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->left)
        {
          current = current->parent;
          RedBlackTree_RightRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_LeftRotate(current->parent->parent);
      }      
    }
  }
  redBlackTree->rootNode->color = 1;
}

//Arrange the tree after deletion
void RedBlackTree_DeleteFix(RedBlackNode *current)
{
  //case 0: if current is red, change it to black 
  //        if current is root, change it to black
  
  while(current != redBlackTree->rootNode && current->color == 1)
  {
    //if current is leftchild
    if(current == current->parent->left)
    {
      RedBlackNode *sibling = current->parent->right;
      
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_LeftRotate(current->parent);
        sibling = current->parent->right;        
      }
      
      //Enter case 2, 3 and 4 where sibling is black
      //case 2: if the two childs of sibling are black
      if(sibling->left->color == 1 && sibling->right->color ==1)
      {
        sibling->color = 0;
        //if current is updated to be root, exit the while loop
        current = current->parent;        
      }
      
      //case 3 and 4: there is only one child of current is black
      else
      {
        //case 3: sibling's right child is black and left child is red
        if(sibling->right->color == 1)
        {
            sibling->left->color = 1;
            sibling->color = 0;
            RedBlackTree_RightRotate(sibling);
            sibling = current->parent->right;
        }
        
        //case 4: sibling's left child is black and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        RedBlackTree_LeftRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }
    }
    //current is right child
    else
    {
      RedBlackNode *sibling = current->parent->left;
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_RightRotate(current->parent);
        sibling = current->parent->left;        
      }
      //Enter case 2, 3 and 4 where sibling is black
      else
      {
        //case 3: sibling's left child is black and right child is red
        if(sibling->left->color == 1)
        {
          sibling->right->color = 1;
          sibling->color = 0;
          RedBlackTree_LeftRotate(sibling);
          sibling = current->parent->left;  
        }
        //case 4: sibling's left child is red and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        sibling->left->color = 1;
        RedBlackTree_RightRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }  
    }    
  }
  current->color = 1; 
}
              
//left rotate about the node
void RedBlackTree_LeftRotate(RedBlackNode *x)
{
   RedBlackNode *y = x->right;
   
   x->right = y->left;
   
   if(y->left != redBlackTree->neel)
   {
     y->left->parent = x;
   }
   
   y->parent = x->parent;
   
   if(x->parent == redBlackTree->neel)
   {
     redBlackTree->rootNode = y;
   }
   else if(x == x->parent->left)
   {
     x->parent->left = y;
   }
   else
   {
     x->parent->right = y;     
   }
   
   y -> left = x;
   x-> parent = y;
}

//right rotate about the node
void RedBlackTree_RightRotate(RedBlackNode *y)
{
  RedBlackNode *x = y->left;
  
  y->left = x->right;
  
  if(x->right != redBlackTree->neel)
  {
    x->right->parent = y;
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
    y->parent->right = x;    
  }
  
  x->right  = y;
  y->parent = x;
}

//Recursively print nodes
void  BSTRecursiveFinding(RedBlackNode *curNode)
{
  if(curNode == redBlackTree->neel)
  {
      return;
  }
  
  printf("%d\n",curNode->ptr_tcb->period);
  
  BSTRecursiveFinding(curNode->left);
  
  BSTRecursiveFinding(curNode->right);
}

//Get the avaliable leaf node based on ptr tcb
RedBlackNode *BSTFindSlots(OS_TCB* _ptr_tcb)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;
  
  RedBlackNode *temp = (RedBlackNode*)0;
  
  //Root is null
  if(root == (RedBlackNode*)0)
  {
    return (RedBlackNode*)0;    
  }
  
  //start from root
  temp = root;
  
  //Break while loop if the current temp is the leaf node
  while( (temp->left  != redBlackTree->neel) &&
         (temp->right != redBlackTree->neel))
  {
    if(_ptr_tcb->period > temp->ptr_tcb->period)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
  }  
  
  return temp;
}

RedBlackNode* search(OS_TCB *_ptr_tcb)
{
  RedBlackNode *temp = redBlackTree->rootNode;
  
  RedBlackNode *toBeReturned = (RedBlackNode*)0;
  
  while(temp != redBlackTree->neel)
  {
    if(_ptr_tcb->period > temp->ptr_tcb->period)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
    if(temp->ptr_tcb->period == _ptr_tcb->period)
    {
      toBeReturned = temp;
      break;      
    }      
  }  
    
  return toBeReturned;
}

/*
************************************************************************************************************************
*                                                       Stack
************************************************************************************************************************
*/

void stack_init()
{
  OS_ERR        err;
  
  OSMemCreate(  (OS_MEM         *)&Stack_MEM,
                (CPU_CHAR       *)"StackPartition",
                (void           *)&Stack_STORAGE[0],
                (OS_MEM_QTY      ) STACK_MAXSIZE,
                (OS_MEM_SIZE     ) sizeof(OS_MUTEX*),
                (OS_ERR         *)&err);
  
  for (int i = 0; i<STACK_MAXSIZE; i++)
  {
    Stack_STORAGE[i] = (OS_MUTEX *)0;
  }
}

int isempty() 
{
   if(top == -1)
      return 1;
   else
      return 0;
}

int isfull() 
{
   if(top == STACK_MAXSIZE)
      return 1;
   else
      return 0;
}

OS_MUTEX* pop() {
   OS_MUTEX *data;
   if(!isempty()) {
      data = Stack_STORAGE[top];
      top = top - 1;   
      return data;
   } else {
      //printf("Could not retrieve data, Stack is empty.\n");
      return 0;
   }
}

void push(OS_MUTEX *data) {

   if(!isfull()) {
      top = top + 1;   
      Stack_STORAGE[top] = data;
   } else {
      //printf("Could not insert data, Stack is full.\n");
   }
}

/*
**************************************************************************************************************
                                              SCHEDULER TASK
**************************************************************************************************************
*/
void OS_RecTaskCreate(OS_TCB        *_p_tcb,
                      CPU_CHAR      *_p_name,
                      OS_TASK_PTR    _p_task,
                      void          *_p_arg,
                      OS_PRIO        _prio,
                      CPU_STK       *_p_stk_base,
                      CPU_STK_SIZE   _stk_limit,
                      CPU_STK_SIZE   _stk_size,
                      OS_MSG_QTY     _q_size,
                      OS_TICK        _time_quanta,
                      void          *_p_ext,
                      OS_OPT         _opt,
                      OS_ERR        *_p_err,
                      CPU_INT32U     _period,
                      CPU_INT32U     _time_remaining)
{
    OS_ERR err;
    
    Temp_TCB *temp_tcb = (Temp_TCB *)OSMemGet((OS_MEM*)&OS_Temp_MEM, (OS_ERR*)&err);
    
    temp_tcb->p_tcb             = _p_tcb;
    temp_tcb->p_name            = _p_name;
    temp_tcb->p_task            = _p_task;
    temp_tcb->p_arg             = _p_arg;
    temp_tcb->prio              = _prio;
    temp_tcb->p_stk_base        = _p_stk_base;
    temp_tcb->stk_limit         = _stk_limit;
    temp_tcb->stk_size          = _stk_size;
    temp_tcb->q_size            = _q_size;
    temp_tcb->time_quanta       = _time_quanta;
    temp_tcb->p_ext             = _p_ext;
    temp_tcb->opt               = _opt;
    temp_tcb->p_err             = _p_err;
    temp_tcb->time_remaining    = _time_remaining;
    temp_tcb->period            = _period;
    temp_tcb->p_tcb->period     = _time_remaining;
    temp_tcb->p_tcb->time_remaining = _time_remaining;
    
    //Insert into AVL Tree
    roots = _insert(roots, temp_tcb);
}

void one_time_call(void)
{
        temp_node *temp = roots;
        one_time_insertion(temp);
}

void execute_task(void)
{
    OS_TCB *temp = Heap_pop();
    if(temp != 0)
    {
        OS_PrioInsert(temp->Prio);
        OS_RdyListInsertTail(temp);
    }
}

void one_time_insertion(temp_node* curNode)
{
    OS_ERR err;
    
    if(curNode == (temp_node*)0)
            return;

    OSSchedLock(&err);
    Temp_TCB *tempo_tcb = (Temp_TCB*)0;
    tempo_tcb = curNode->data;
              
    OS_RecTaskBinaryHeapCreate(
                 (OS_TCB        *) tempo_tcb->p_tcb,
                 (CPU_CHAR      *) tempo_tcb->p_name, 
                 (OS_TASK_PTR    ) tempo_tcb->p_task, 
                 (void          *) tempo_tcb->p_arg, 
                 (OS_PRIO        ) tempo_tcb->prio, 
                 (CPU_STK       *) tempo_tcb->p_stk_base, 
                 (CPU_STK_SIZE   ) tempo_tcb->stk_limit, 
                 (CPU_STK_SIZE   ) tempo_tcb->stk_size, 
                 (OS_MSG_QTY     ) tempo_tcb->q_size, 
                 (OS_TICK        ) tempo_tcb->time_quanta, 
                 (void          *) (CPU_INT32U) tempo_tcb->p_ext, 
                 (OS_OPT         )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR        *) &err,
                 (CPU_INT32U     ) tempo_tcb->time_remaining);
              
   Heap_Insert(tempo_tcb->p_tcb);
   execute_task();
   
   //By in-order traversal
   one_time_insertion(curNode->left);
   one_time_insertion(curNode->right);
   
   OSSchedUnlock(&err);
}

void basic(void)
{
    OS_ERR      err;
    CPU_TS      ts;
    OS_SEM_CTR  ctr;
    int count = 0;

    while(1)
    {
        //Waiting for Semaphore
        ctr = OSSemPend(&sema,0,OS_OPT_PEND_BLOCKING,&ts,&err);
        
        //Only run once
        if(count == 0)
        {
            //insert the 5 task into the Binary Heap at time 0
            one_time_call();
            count++;
        }
        
        //Update time remaining
        AVLTree_Update_Time();
    }
}

// Basic Task for while loop
void OS_Recstart(void)
{
    OS_ERR err;
    OSTaskCreate(
                 (OS_TCB     *)&BasicTaskTCB, 
                 (CPU_CHAR   *)"TaskScheduler", 
                 (OS_TASK_PTR ) basic, 
                 (void       *) 0, 
                 (OS_PRIO     ) 2u, 
                 (CPU_STK    *) &BasicTaskStk[0], 
                 (CPU_STK_SIZE) BasicTask_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) BasicTask_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *) (CPU_INT32U) 2, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err
    );   
}

/*
*********************************************************************************************************
                               CREATE RECURSIVE TASK FOR BINARY HEAP
*********************************************************************************************************
*/
void  OS_RecTaskBinaryHeapCreate (OS_TCB        *p_tcb,
                                  CPU_CHAR      *p_name,
                                  OS_TASK_PTR    p_task,
                                  void          *p_arg,
                                  OS_PRIO        prio,
                                  CPU_STK       *p_stk_base,
                                  CPU_STK_SIZE   stk_limit,
                                  CPU_STK_SIZE   stk_size,
                                  OS_MSG_QTY     q_size,
                                  OS_TICK        time_quanta,
                                  void          *p_ext,
                                  OS_OPT         opt,
                                  OS_ERR        *p_err,
                                  CPU_INT32U     period)
{
    CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
    if (prio >= OS_CFG_PRIO_MAX) {                          /* Priority must be within 0 and OS_CFG_PRIO_MAX-1        */
        *p_err = OS_ERR_PRIO_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
    
    p_tcb->period        = period;
    
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */

                                                            /* --------------- ADD TASK TO READY LIST --------------- */
    OS_CRITICAL_ENTER();
    
#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListAdd(p_tcb);
#endif

    OSTaskQty++;                                            /* Increment the #tasks counter                           */

    if (OSRunning != OS_STATE_OS_RUNNING) {                 /* Return if multitasking has not started                 */
        OS_CRITICAL_EXIT();
        return;
    }

    OS_CRITICAL_EXIT_NO_SCHED(); 
}

/*
**********************************************************************************
                                MUTEX
**********************************************************************************
*/

void  OSMutexCreates (OS_MUTEX    *p_mutex,
                     CPU_CHAR    *p_name,
                     OS_ERR      *p_err,
                     CPU_INT32U   ceiling)
{
    CPU_SR_ALLOC();

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to be called from an ISR                   */
        *p_err = OS_ERR_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    p_mutex->Type              =  OS_OBJ_TYPE_MUTEX;        /* Mark the data structure as a mutex                     */
    p_mutex->NamePtr           =  p_name;
    p_mutex->OwnerTCBPtr       = (OS_TCB       *)0;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)0;         /* Mutex is available                                     */
    p_mutex->TS                = (CPU_TS        )0;
    p_mutex->OwnerOriginalPrio =  OS_CFG_PRIO_MAX;
    
    /*          ADDED */
    p_mutex->Ceiling           = ceiling;
    
    OS_PendListInit(&p_mutex->PendList);                    /* Initialize the waiting list                            */

#if OS_CFG_DBG_EN > 0u
    OS_MutexDbgListAdd(p_mutex);
#endif
    OSMutexQty++;

    CPU_CRITICAL_EXIT();
    *p_err = OS_ERR_NONE;
}

/*
*****************************************************************************************
                                          PEND
*****************************************************************************************
*/

void  OSMutexPends (OS_MUTEX   *p_mutex,
                   OS_TICK     timeout,
                   OS_OPT      opt,
                   CPU_TS     *p_ts,
                   OS_ERR     *p_err)
{
    OS_PEND_DATA  pend_data;
    OS_TCB       *p_tcb;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_PEND_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate arguments                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
    switch (opt) {
        case OS_OPT_PEND_BLOCKING:
        case OS_OPT_PEND_NON_BLOCKING:
             break;

        default:
             *p_err = OS_ERR_OPT_INVALID;
             return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    if (p_ts != (CPU_TS *)0) {
       *p_ts  = (CPU_TS  )0;                                /* Initialize the returned timestamp                      */
    }

    CPU_CRITICAL_ENTER();
    
    /* if resource available and the current task prio greater than system ceiling, then grant access to mutex*/
    if (p_mutex->OwnerNestingCtr == (OS_NESTING_CTR)0 && OSTCBCurPtr->Prio < sys_ceil)
    { 
          p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;          /* Yes, caller may proceed                                */
          p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
          p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
          if (p_ts != (CPU_TS *)0) {
             *p_ts                   = p_mutex->TS;
          }
          
          //start_time = OS_TS_GET();
          push(p_mutex);          /*Push to the stack */
          /*start_time2 = OS_TS_GET();
          overall_time = start_time2 - start_time;
          overall_time = overall_time/50;
          printf("Stack PUSH for task %d Timing: %d us\n",OSTCBCurPtr->Prio,overall_time);*/
          sys_ceil = p_mutex->Ceiling; /* Update system ceiling */
          
          CPU_CRITICAL_EXIT();
          *p_err                     =  OS_ERR_NONE;
          return;
    }
    /* Two cases:
         1. if resource available, but the current task priority is equal or smaller than system ceiling
         2. resource not available*/
    else
    {   /* Check whether the current task already hold a mutex or not */
        for(int i = top; i >= -1; i--) /* Decending stack */
        {
           if(OSTCBCurPtr == Stack_STORAGE[i]->OwnerTCBPtr)
           {
                  second = 1;
           }
        }
        
        /* Current task holding to another mutex, grant access to this mutex */
        if(second == 1)
        {
            if (p_mutex->OwnerNestingCtr == (OS_NESTING_CTR)0) /* Resource available?                                    */
            { 
              p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;          /* Yes, caller may proceed                                */
              p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
              p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
              if (p_ts != (CPU_TS *)0) {
                 *p_ts                   = p_mutex->TS;
              }
              
              //start_time = OS_TS_GET();
              push(p_mutex);          /*Push to the stack */
              /*start_time2 = OS_TS_GET();
              overall_time = start_time2 - start_time;
              overall_time = overall_time/50;
              printf("Stack PUSH for task %d Timing: %d us\n",OSTCBCurPtr->Prio,overall_time);*/
              sys_ceil = p_mutex->Ceiling; /* Update system ceiling */
              
              CPU_CRITICAL_EXIT();
              *p_err                     =  OS_ERR_NONE;
              second = 0;
              return;
            }
        }
        else /*Resource not available*/
        {
          //Insert to Block List
          //start_time = OS_TS_GET();
          RedBlackTree_insert(OSTCBCurPtr);
          /*start_time2 = OS_TS_GET();
          overall_time = start_time2 - start_time;
          overall_time = overall_time/50;
          printf("RBT Insert Timing: %d us\n",overall_time);*/
          element++;
        }
    }   

    if (OSTCBCurPtr == p_mutex->OwnerTCBPtr) {              /* See if current task is already the owner of the mutex  */
        p_mutex->OwnerNestingCtr++;
        if (p_ts != (CPU_TS *)0) {
           *p_ts  = p_mutex->TS;
        }
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_OWNER;                        /* Indicate that current task already owns the mutex      */
        return;
    }

    if ((opt & OS_OPT_PEND_NON_BLOCKING) != (OS_OPT)0) {    /* Caller wants to block if not available?                */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_PEND_WOULD_BLOCK;                   /* No                                                     */
        return;
    } else {
        if (OSSchedLockNestingCtr > (OS_NESTING_CTR)0) {    /* Can't pend when the scheduler is locked                */
            CPU_CRITICAL_EXIT();
            *p_err = OS_ERR_SCHED_LOCKED;
            return;
        }
    }

    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();                  /* Lock the scheduler/re-enable interrupts                */
    p_tcb = p_mutex->OwnerTCBPtr;                           /* Point to the TCB of the Mutex owner                    */
    if (p_tcb->Prio > OSTCBCurPtr->Prio) {                  /* See if mutex owner has a lower priority than current   */
        switch (p_tcb->TaskState) {
            case OS_TASK_STATE_RDY:
                 OS_RdyListRemove(p_tcb);                   /* Remove from ready list at current priority             */
                 p_tcb->Prio = OSTCBCurPtr->Prio;           /* Raise owner's priority                                 */
                 OS_PrioInsert(p_tcb->Prio);
                 OS_RdyListInsertHead(p_tcb);               /* Insert in ready list at new priority                   */
                 break;

            case OS_TASK_STATE_DLY:
            case OS_TASK_STATE_DLY_SUSPENDED:
            case OS_TASK_STATE_SUSPENDED:
                 p_tcb->Prio = OSTCBCurPtr->Prio;           /* Only need to raise the owner's priority                */
                 break;

            case OS_TASK_STATE_PEND:                        /* Change the position of the task in the wait list       */
            case OS_TASK_STATE_PEND_TIMEOUT:
            case OS_TASK_STATE_PEND_SUSPENDED:
            case OS_TASK_STATE_PEND_TIMEOUT_SUSPENDED:
                 OS_PendListChangePrio(p_tcb,
                                       OSTCBCurPtr->Prio);
                 break;

            default:
                 OS_CRITICAL_EXIT();
                 *p_err = OS_ERR_STATE_INVALID;
                 return;
        }
    }

    OS_Pend(&pend_data,                                     /* Block task pending on Mutex                            */
            (OS_PEND_OBJ *)((void *)p_mutex),
             OS_TASK_PEND_ON_MUTEX,
             timeout);

    OS_CRITICAL_EXIT_NO_SCHED();

    OSSched();                                              /* Find the next highest priority task ready to run       */

    CPU_CRITICAL_ENTER();
    switch (OSTCBCurPtr->PendStatus) {
        case OS_STATUS_PEND_OK:                             /* We got the mutex                                       */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_NONE;
             break;

        case OS_STATUS_PEND_ABORT:                          /* Indicate that we aborted                               */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_PEND_ABORT;
             break;

        case OS_STATUS_PEND_TIMEOUT:                        /* Indicate that we didn't get mutex within timeout       */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = (CPU_TS  )0;
             }
             *p_err = OS_ERR_TIMEOUT;
             break;

        case OS_STATUS_PEND_DEL:                            /* Indicate that object pended on has been deleted        */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_OBJ_DEL;
             break;

        default:
             *p_err = OS_ERR_STATUS_INVALID;
             break;
    }
    CPU_CRITICAL_EXIT();
}

/*
*****************************************************************************************
                                      POST
*****************************************************************************************
*/

void  OSMutexPosts (OS_MUTEX  *p_mutex,
                   OS_OPT     opt,
                   OS_ERR    *p_err)
{
    OS_PEND_LIST  *p_pend_list;
    OS_TCB        *p_tcb;
    CPU_TS         ts;
    CPU_SR_ALLOC();

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_POST_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    if (OSTCBCurPtr != p_mutex->OwnerTCBPtr) {              /* Make sure the mutex owner is releasing the mutex       */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_NOT_OWNER;
        return;
    }
    
    /*-------------------------------ADDED---------------------------------0---*/
    //start_time = OS_TS_GET();
    pop(p_mutex); /* Pop off the stack */
    /*start_time2 = OS_TS_GET();
    overall_time = start_time2 - start_time;
    overall_time = overall_time/50;
    printf("Stack POP for task %d Timing: %d us\n",OSTCBCurPtr->Prio,overall_time);*/
    
    /* Update system ceiling */
    if(isempty() == 0) /* Not empty */
    {
       sys_ceil = Stack_STORAGE[top]->Ceiling; /* Assign system ceiling to the top element  */
       for(int i = top-1; i > -1; i--) /* Iterate through the whole stack to find the largest priority mutex */
       {
          int tempRC;
          tempRC = Stack_STORAGE[i]->Ceiling;
          if(sys_ceil > tempRC) /* Finding the biggest priority mutex */
          {
              sys_ceil = tempRC; 
          }
       }
    }
    else /*if no available task to run next, set system ceiling to a very low priority */
      sys_ceil = 35; 
    
    RedBlackNode* temp;
    if(element > 0)
    {
      temp = LeftMost(redBlackTree->rootNode);
      if(temp != (RedBlackNode*)0)
      {
         if(temp->ptr_tcb->period < sys_ceil)
         {
             /* Grant resource */
             if (p_mutex->OwnerNestingCtr == (OS_NESTING_CTR)0) /* Resource available?                                    */
              { 
                p_mutex->OwnerTCBPtr       =  temp->ptr_tcb;          /* Yes, caller may proceed                                */
                p_mutex->OwnerOriginalPrio =  temp->ptr_tcb->Prio;
                p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
                
                //start_time = OS_TS_GET();
                push(p_mutex);          /*Push to the stack */
                /*start_time2 = OS_TS_GET();
                overall_time = start_time2 - start_time;
                overall_time = overall_time/50;
                printf("Stack PUSH for task %d Timing: %d us\n",OSTCBCurPtr->Prio,overall_time);*/
                sys_ceil = p_mutex->Ceiling; /* Update system ceiling */
                
                CPU_CRITICAL_EXIT();
                *p_err                     =  OS_ERR_NONE;
                second = 0;
              }
               //start_time = OS_TS_GET();
               RedBlackTree_delete(temp->ptr_tcb);
               /*start_time2 = OS_TS_GET();
               overall_time = start_time2 - start_time;
               overall_time = overall_time/50;
               printf("RBT Deletion Timing: %d us\n",overall_time);*/
               Heap_Insert(temp->ptr_tcb);
               element--;
         }
      }
    }//Else, do nothing
    

    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();
    ts          = OS_TS_GET();                              /* Get timestamp                                          */
    p_mutex->TS = ts;
    p_mutex->OwnerNestingCtr--;                             /* Decrement owner's nesting counter                      */
    if (p_mutex->OwnerNestingCtr > (OS_NESTING_CTR)0) {     /* Are we done with all nestings?                         */
        OS_CRITICAL_EXIT();                                 /* No                                                     */
        *p_err = OS_ERR_MUTEX_NESTING;
        return;
    }

    p_pend_list = &p_mutex->PendList;
    if (p_pend_list->NbrEntries == (OS_OBJ_QTY)0) {         /* Any task waiting on mutex?                             */
        p_mutex->OwnerTCBPtr     = (OS_TCB       *)0;       /* No                                                     */
        p_mutex->OwnerNestingCtr = (OS_NESTING_CTR)0;
        OS_CRITICAL_EXIT();
        *p_err = OS_ERR_NONE;
        return;
    }
                                                            /* Yes                                                    */
    if (OSTCBCurPtr->Prio != p_mutex->OwnerOriginalPrio) {
        OS_RdyListRemove(OSTCBCurPtr);
        OSTCBCurPtr->Prio = p_mutex->OwnerOriginalPrio;     /* Lower owner's priority back to its original one        */
        OS_PrioInsert(OSTCBCurPtr->Prio);
        OS_RdyListInsertTail(OSTCBCurPtr);                  /* Insert owner in ready list at new priority             */
        OSPrioCur         = OSTCBCurPtr->Prio;
    }
                                                            /* Get TCB from head of pend list                         */
    p_tcb                      = p_pend_list->HeadPtr->TCBPtr;
    p_mutex->OwnerTCBPtr       = p_tcb;                     /* Give mutex to new owner                                */
    p_mutex->OwnerOriginalPrio = p_tcb->Prio;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
                                                            /* Post to mutex                                          */
    OS_Post((OS_PEND_OBJ *)((void *)p_mutex),
            (OS_TCB      *)p_tcb,
            (void        *)0,
            (OS_MSG_SIZE  )0,
            (CPU_TS       )ts);

    OS_CRITICAL_EXIT_NO_SCHED();

    if ((opt & OS_OPT_POST_NO_SCHED) == (OS_OPT)0) {
        OSSched();                                          /* Run the scheduler                                      */
    }

    *p_err = OS_ERR_NONE;
}
                  