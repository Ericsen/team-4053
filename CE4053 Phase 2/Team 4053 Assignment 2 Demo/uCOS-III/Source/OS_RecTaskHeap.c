/*
************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************
*/
#include "OS_RecTask.h"

/*
************************************************************************************************************************
*                                                 GLOBAL VARIABLE
************************************************************************************************************************
*/
//Global Variable

#define CMP(a, b) ((a) <= (b))
//static const unsigned int base_size = 20;

//Binary Heap
OS_MEM			HeapNodeMEM;
HeapNode   		alloMemHeapNode[20];

//Heap Tree
OS_MEM			HeapTreeMem;
HeapTree		alloMemHeapTree[1];

//Heap Tree Reference
HeapTree *heapTree = (HeapTree *)0;

//OS TCB
OS_MEM			OS_TCB_MEM;
OS_USER_TCB 	                alloMem_OS_TCB[40];

/*
************************************************************************************************************************
*                                                 Binary Heap
************************************************************************************************************************
*/

// Prepares the heap for use
void BinaryHeap_init(void)
{	
	
	OS_ERR err;
	
	printf ("Initialize Binary Heap\n");
	
	//Allocate Memory to Binary Heap Node
	OSMemCreate(
		(OS_MEM		*) &HeapNodeMEM,
		(CPU_CHAR	*) "Heap Node Partition",
		(void		*) &alloMemHeapNode,
		(OS_MEM_QTY  ) 20,
		(OS_MEM_SIZE ) sizeof(HeapNode),
		(OS_ERR		*) &err
	);
	
	//allocate memory to binary heap tree
	OSMemCreate(
		(OS_MEM		*) &HeapTreeMem,
		(CPU_CHAR	*) "Heap Tree Partition",
		(void		*) &alloMemHeapTree,
		(OS_MEM_QTY  ) 1,
		(OS_MEM_SIZE ) sizeof(HeapTree),
		(OS_ERR		*) &err
	);
	
	
	//initialize the node
	heapTree = (HeapTree*) OSMemGet((OS_MEM	*)&HeapTreeMem,
					(OS_ERR *)&err);
	
	heapTree->root = (HeapNode*)0;
	heapTree->data = &alloMemHeapNode[0];
	
	int i = 0;
	while (i < 20)
	{
		alloMemHeapNode[i].ptr_tcb = (OS_TCB*)0;
		i++;
	}
	
	heapTree->count = (CPU_INT08U) 0;
	heapTree->size = (CPU_INT08U )20;
		
}

// Inserts element to the heap
void BinaryHeap_Insert(OS_TCB* _ptr_tcb)
{
	//get total number of the element in the list
	CPU_INT08U count = heapTree->count;
	
	//add the TCB into the data structure
	alloMemHeapNode[count].ptr_tcb = _ptr_tcb;
	
	//update the number of element
	count++;
	
	heapTree->count = count;
	
}

/*
void BinaryHeap_Heapify(void)
{
    //Gets current number of tasks
    CPU_INT08U count = heapTree->count;
    CPU_INT08U i = 0;
    HeapNode *lists  = &alloMemHeapNode[0];
    while(i < count)
    {
      BinaryHeap_heapify(i,lists);
      i++;
    }
}
*/
/*
//PrintTree
void BinaryHeap_printTree(void)
{
    //Gets current number of tasks
    CPU_INT08U count = heapTree->count;
    CPU_INT08U i = 0;
    HeapNode *lists = heapTree->data;
    while(i<count)
    {
      printf("%d\n",lists[i].ptr_tcb->period);
      i++;
    }  
}
*/
//Delete particular Nodes
void BinaryHeap_deleteNode(OS_TCB *ptr_tcb)
{
  
  //Get readyList
  HeapNode *lists = heapTree->data;
  //Search index of deleted Node
  CPU_INT08U deletedNode = BinaryHeap_search(ptr_tcb);
  //Gets current number of tasks
  CPU_INT08U count = heapTree->count;  
  
  if(deletedNode!= (CPU_INT08U)255)
  {
    BinaryHeap_swap(deletedNode,count-1);
  }
  
  lists[count-1].ptr_tcb = (OS_TCB*)0;
  //BinaryHeap_Heapify();
  count--;
  heapTree->count = count;
}

//Pop the minimum node and heapify
OS_TCB* BinaryHeap_popMinimum()
{
  //Get readyList
  HeapNode *lists = heapTree->data;
  OS_TCB* toBeReturned = lists[0].ptr_tcb;
  if(toBeReturned != 0)
  {
    BinaryHeap_deleteNode(toBeReturned);
    return toBeReturned;
  }
  else
  {
    return (OS_TCB*)0;
  }
}

/*
*  Utilities
*/

//Return index of Nodes 
CPU_INT08U BinaryHeap_search(OS_TCB *ptr_tcb)
{
    //Gets current number of tasks
    CPU_INT08U count= heapTree->count;
    CPU_INT08U i = 0;
    HeapNode *lists = heapTree->data;
    while(i<count)
    {
      if(lists[i].ptr_tcb == ptr_tcb)
      {
        return i;
      }
      i++;
    }    
    return 255;
}

//Return LeftNodeIndex
CPU_INT08U BinaryHeap_leftNodeIndex(CPU_INT08U i)
{
    return i * 2 + 1;
}
  
//Return RightNodeIndex
CPU_INT08U BinaryHeap_rightNodeIndex(CPU_INT08U i)
{
    return i * 2 + 2;           
}

//Recursiviliy Heapify
/*
void BinaryHeap_heapify(CPU_INT08U i,HeapNode *lists)
{
   if(i > 20)
      return;
   
   CPU_INT08U parent_Index = i;
   CPU_INT08U left_Index = BinaryHeap_leftNodeIndex(i);
   CPU_INT08U right_Index = BinaryHeap_rightNodeIndex(i);
   
   //if left node is valid
    if(left_Index < 20 && lists[left_Index].ptr_tcb != (OS_TCB *)0)
   {
     //Coniture go to the leftmost
     BinaryHeap_heapify(left_Index,lists);
     
     //If right is valid
      if(right_Index < 20 && lists[right_Index].ptr_tcb != (OS_TCB *)0)
     {
         if(lists[left_Index].ptr_tcb->period < lists[right_Index].ptr_tcb->period)
        {
           if(lists[left_Index].ptr_tcb->period < lists[parent_Index].ptr_tcb->period)
          {
            BinaryHeap_swap(left_Index,parent_Index);
          }
        }
        else
        {
           if(lists[right_Index].ptr_tcb->period < lists[parent_Index].ptr_tcb->period)
          {
            BinaryHeap_swap(right_Index,parent_Index);
          }         
        }
     }
     else
     {
       return;
     }
   }
   
   if(right_Index < 20)
   {
     BinaryHeap_heapify(right_Index,lists);
   }
}
*/

//Swap the contents
void BinaryHeap_swap(CPU_INT08U i1,CPU_INT08U i2)
{
   OS_TCB *temp =  heapTree->data[i1].ptr_tcb;
   heapTree->data[i1].ptr_tcb =  heapTree->data[i2].ptr_tcb;
   heapTree->data[i2].ptr_tcb = temp;
}
/*
************************************************************************************************************************
*                                                 Recursive Task API
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/
void OSRec_Init(void)
{
  //ReadyList
  BinaryHeap_init();

}

//Create recursive Task Node
void  OSRec_TaskCreate (OS_TCB        *_p_tcb,
                    CPU_CHAR      *_p_name,
                    OS_TASK_PTR    _p_task,
                    void          *_p_arg,
                    OS_PRIO        _prio,
                    CPU_STK       *_p_stk_base,
                    CPU_STK_SIZE   _stk_limit,
                    CPU_STK_SIZE   _stk_size,
                    OS_MSG_QTY     _q_size,
                    OS_TICK        _time_quanta,
                    void          *_p_ext,
                    OS_OPT         _opt,
                    OS_ERR        *_p_err,
                    CPU_INT32U     _period,
                    CPU_INT32U     _timeRemaining)
{
   OS_ERR err;
   
   OS_USER_TCB *os_tcb = (OS_USER_TCB *)OSMemGet((OS_MEM *)&OS_TCB_MEM,
                                       (OS_ERR    *)&err);
    
   os_tcb->p_tcb         = _p_tcb;
   os_tcb->p_name        = _p_name; 
   os_tcb->p_task        = _p_task;
   os_tcb->p_arg         = _p_arg;
   os_tcb->prio          = _prio;
   os_tcb->p_stk_base    = _p_stk_base;
   os_tcb->stk_limit     = _stk_limit;
   os_tcb->stk_size      = _stk_size;
   os_tcb->q_size        = _q_size;
   os_tcb->time_quanta   = _time_quanta;
   os_tcb->p_ext         = _p_ext;
   os_tcb->opt           = _opt;
   os_tcb->p_err         = _p_err;
   os_tcb->timeRemaining = _timeRemaining;
   os_tcb->period        = _period;
   
   BinaryHeap_Insert(os_tcb->p_tcb);
}

//Delete Task From binary Heap
void OSRec_TaskDel(OS_TCB* ptr_tcb)
{
    OS_TCB* highestPrio = BinaryHeap_popMinimum();
    
    OS_PrioInsert(highestPrio->Prio);
    OS_RdyListInsertTail(highestPrio);
}

//Scheduler Task
void scheduler(void)
{
  OS_ERR      err;
  CPU_TS      ts;
  OS_SEM_CTR  ctr;
  
  while(1)
  {
    //Pend
    ctr = OSSemPend(&SwSem,0,OS_OPT_PEND_BLOCKING,&ts,&err);
    printf("Scheduler triggered\n");
  }
}

//Rec Task scheduler
void OSRec_start(void)
{
    OS_ERR err;
    OSTaskCreate(
                 (OS_TCB     *)&AppTaskDispatcherTCB, 
                 (CPU_CHAR   *)"TaskScheduler", 
                 (OS_TASK_PTR ) scheduler, 
                 (void       *) 0, 
                 (OS_PRIO     ) 3u, 
                 (CPU_STK    *) &BasicTaskStk[0], 
                 (CPU_STK_SIZE) AppTask_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) AppTask_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *) (CPU_INT32U) 2, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err
    );   
    printf("Recursive Task created\n");
}


//Task Create Called by Binary Tree
void  OSRec_TaskCBinaryTreeCreate (OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err)
{
    CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
    if (prio >= OS_CFG_PRIO_MAX) {                          /* Priority must be within 0 and OS_CFG_PRIO_MAX-1        */
        *p_err = OS_ERR_PRIO_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */

                                                            /* --------------- ADD TASK TO READY LIST --------------- */
    OS_CRITICAL_ENTER();

#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListAdd(p_tcb);
#endif

    OSTaskQty++;                                            /* Increment the #tasks counter                           */

    if (OSRunning != OS_STATE_OS_RUNNING) {                 /* Return if multitasking has not started                 */
        OS_CRITICAL_EXIT();
        return;
    }

    OS_CRITICAL_EXIT_NO_SCHED(); 
}


