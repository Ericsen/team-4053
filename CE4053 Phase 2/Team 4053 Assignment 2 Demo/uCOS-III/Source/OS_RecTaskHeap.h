
/*
************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************
*/
#include <os.h>
#include <stdio.h>
#include <stdlib.h>

/*
************************************************************************************************************************
*                                                 DATA STRUCTURE
************************************************************************************************************************
*/
typedef struct _heapNode{
	OS_TCB* ptr_tcb;
}HeapNode;

typedef struct _heaptree
{
	HeapNode *root;
	HeapNode *data; // Array with the elements
	CPU_INT08U size; // Size of the allocated memory (in number of items)
	CPU_INT08U count; // Count of the elements in the heap
} HeapTree;

typedef struct OS_USER_TCB
{
                    OS_TCB        *p_tcb;
                    CPU_CHAR      *p_name;
                    OS_TASK_PTR    p_task;
                    void          *p_arg;
                    OS_PRIO        prio;
                    CPU_STK       *p_stk_base;
                    CPU_STK_SIZE   stk_limit;
                    CPU_STK_SIZE   stk_size;
                    OS_MSG_QTY     q_size;
                    OS_TICK        time_quanta;
                    void          *p_ext;
                    OS_OPT         opt;
                    OS_ERR        *p_err;
                    CPU_INT32U     period;
                    CPU_INT32U     timeRemaining;
}OS_USER_TCB;


/*
************************************************************************************************************************
*                                                 HEAP FUNCTION PROTOTYPE
************************************************************************************************************************
*/

//Allocate memory block and initialize
void BinaryHeap_init(void);
//Insert Task TCB into the binary heap
void BinaryHeap_Insert(OS_TCB *_ptr_tcb);
//Heapify the structure
void BinaryHeap_Heapify(void);

//print trees
void BinaryHeap_printTree(void);

//delete Nodes
void BinaryHeap_deleteNode(OS_TCB *ptr_tcb);

//Pop the minimum node and heapify
OS_TCB* BinaryHeap_popMinimum();

//Helper Function
CPU_INT08U BinaryHeap_search(OS_TCB *ptr_tcb);             //Return -1 if not found
CPU_INT08U BinaryHeap_leftNodeIndex(CPU_INT08U index);
CPU_INT08U BinaryHeap_rightNodeIndex(CPU_INT08U index);
void BinaryHeap_heapify(CPU_INT08U index,HeapNode *lists);
void BinaryHeap_swap(CPU_INT08U index1,CPU_INT08U index2);

/*
************************************************************************************************************************
*                                                 Recursive Task API
************************************************************************************************************************
*/

void OS_RecInit(void);

void  OS_RecTaskCreate (OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err,
                    CPU_INT32U     period,
                    CPU_INT32U     timeRemaining);

void  OSRec_TaskHeapCreate 
					(OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err);


void OS_RecTaskDelete(OS_TCB* ptr_tcb);

//BasicTask Global variable

#define  AppTask_STK_SIZE                  128u

static  OS_TCB       AppTaskDispatcherTCB;
static  CPU_STK      BasicTaskStk[AppTask_STK_SIZE];
static void          AppTaskDispatcher(void  *p_arg);

//Start Rec
void OS_RecStart(void);