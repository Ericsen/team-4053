#include <os.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct _Temp_TCB{
    OS_TCB        *p_tcb;
    CPU_CHAR      *p_name;
    OS_TASK_PTR    p_task;
    void          *p_arg;
    OS_PRIO        prio;
    CPU_STK       *p_stk_base;
    CPU_STK_SIZE   stk_limit;
    CPU_STK_SIZE   stk_size;
    OS_MSG_QTY     q_size;
    OS_TICK        time_quanta;
    void          *p_ext;
    OS_OPT         opt;
    OS_ERR        *p_err;
    CPU_INT32U     period;
    CPU_INT32U     time_remaining;
}Temp_TCB;

typedef struct nodes
{
	OS_TCB *data;
	struct nodes *left, *right;
	int ht;
}node;

typedef struct temp_nodes
{
        Temp_TCB *data;
        struct temp_nodes *left, *right;
        int ht;
}temp_node;

// prototype for heap
typedef struct _heapNode{
	OS_TCB* ptr_tcb;
}HeapNode;

typedef struct _heaptree
{
	HeapNode *root;
	HeapNode *data; // Array with the elements
	CPU_INT08U size; // Size of the allocated memory (in number of items)
	CPU_INT08U count; // Count of the elements in the heap
} HeapTree;

/*
***********************************************************************************************
                                    AVL FUNCTION PROTOTYPE
***********************************************************************************************
*/
void print(temp_node * T);
void deleted(void);
void display(void);
void AVLTree_Update_Time();
void AVLTree_Recursive_Update(temp_node* curNode);
temp_node *_insert(temp_node* T, Temp_TCB *x);
temp_node *_Delete(temp_node* T, int x);
temp_node *_search(temp_node* T, int key);
void _inorder(temp_node * T);
int _height(temp_node * T);
temp_node *_rotateright(temp_node * x);
temp_node *_rotateleft(temp_node * x);
temp_node *_RR(temp_node * T);
temp_node *_LL(temp_node * T);
temp_node *_LR(temp_node * T);
temp_node *_RL(temp_node * T);
int _BF(temp_node * T);
void Insert_To_Binary_Heap(temp_node* curNode);

void one_time_insertion(temp_node* curNode);
void one_time_call(void);
void execute_task(void);

/*
************************************************************************************************************************
*                                                 HEAP FUNCTION PROTOTYPE
************************************************************************************************************************
*/

//Allocate memory block and initialize
void BinaryHeap_init(void);

//Insert Task TCB into the binary heap
void Heap_Insert(OS_TCB *_ptr_tcb);

//Heapify the structure
void Heap_Heapify(void);

//print trees
void Heap_printTree(void);

//delete Nodes
void Heap_delete(OS_TCB *ptr_tcb);

//Pop the minimum node and heapify
OS_TCB* Heap_pop();

//Helper Function
CPU_INT08U Heap_search(OS_TCB *ptr_tcb);           //Return -1 if not found
OS_TCB * BH_search(OS_TCB *ptcb);
CPU_INT08U Heap_leftNodeIndex(CPU_INT08U index);
CPU_INT08U Heap_rightNodeIndex(CPU_INT08U index);
void Heap_heapify(CPU_INT08U index,HeapNode *lists);
void Heap_swap(CPU_INT08U index1,CPU_INT08U index2);

#define  BasicTask_STK_SIZE                  128u

static  OS_TCB       BasicTaskTCB;
static  CPU_STK      BasicTaskStk[BasicTask_STK_SIZE];
static void          BasicTask(void  *p_arg);

//Start Rec
void OS_RecStart(void);

/*
******************************************************************************************
                                       OS_RecTask
******************************************************************************************
*/
void    OS_RecTaskCreate          (OS_TCB                *_p_tcb,
                                   CPU_CHAR              *_p_name,
                                   OS_TASK_PTR            _p_task,
                                   void                  *_p_arg,
                                   OS_PRIO                _prio,
                                   CPU_STK               *_p_stk_base,
                                   CPU_STK_SIZE           _stk_limit,
                                   CPU_STK_SIZE           _stk_size,
                                   OS_MSG_QTY             _q_size,
                                   OS_TICK                _time_quanta,
                                   void                  *_p_ext,
                                   OS_OPT                 _opt,
                                   OS_ERR                *_p_err,
                                   CPU_INT32U             _period,
                                   CPU_INT32U             _time_remaining);

void  OS_RecTaskBinaryHeapCreate (OS_TCB        *p_tcb,
                                  CPU_CHAR      *p_name,
                                  OS_TASK_PTR    p_task,
                                  void          *p_arg,
                                  OS_PRIO        prio,
                                  CPU_STK       *p_stk_base,
                                  CPU_STK_SIZE   stk_limit,
                                  CPU_STK_SIZE   stk_size,
                                  OS_MSG_QTY     q_size,
                                  OS_TICK        time_quanta,
                                  void          *p_ext,
                                  OS_OPT         opt,
                                  OS_ERR        *p_err,
                                  CPU_INT32U     period);