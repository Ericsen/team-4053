#include <OS_RecTask.h>

/* root node */
temp_node *roots = (temp_node*)0;

//TEMP TCB
OS_MEM          OS_Temp_MEM;
Temp_TCB        OS_Temp_TCB_STORAGE[40];

//AVL
OS_MEM          OS_TCB_MEM;
OS_TCB          OS_TCB_STORAGE[50];

//AVL memory block
OS_MEM          AVL_MEM;
node            AVL_STORAGE[50];

//Binary Heap
OS_MEM			HeapNodeMEM;
HeapNode   		alloMemHeapNode[20];

//Heap Tree
OS_MEM			HeapTreeMem;
HeapTree		alloMemHeapTree[1];

//Heap Tree Reference
HeapTree *heapTree = (HeapTree *)0;
HeapTree *heapTreeTemp = (HeapTree *)0;

//Counter Variable
int counter = 0;

int start_time = 0;
int start_time2 = 0;
int insertion_time = 0;
int heapifying_time = 0;
int pop_min_time = 0;
int overall_time = 0;

/*
********************************************************************************************
                               Create Memory Partition 
********************************************************************************************
*/
void AVLTree_init(void)
{
      OS_ERR        err;
      
      OSMemCreate(  (OS_MEM         *)&AVL_MEM,
                    (CPU_CHAR       *)"AVLPartition",
                    (void           *)&AVL_STORAGE[0],
                    (OS_MEM_QTY      ) 50,
                    (OS_MEM_SIZE     ) sizeof(node),
                    (OS_ERR         *)&err);
      
      OSMemCreate(  (OS_MEM         *)&OS_TCB_MEM,
                    (CPU_CHAR       *)"OS_TCBPartition",
                    (void           *)&OS_TCB_STORAGE[0],
                    (OS_MEM_QTY      ) 50,
                    (OS_MEM_SIZE     ) sizeof(OS_TCB),
                    (OS_ERR         *)&err);
      
      OSMemCreate(  (OS_MEM         *)&OS_Temp_MEM,
                    (CPU_CHAR       *)"OS_TempPartition",
                    (void           *)&OS_Temp_TCB_STORAGE[0],
                    (OS_MEM_QTY      ) 50,
                    (OS_MEM_SIZE     ) sizeof(Temp_TCB),
                    (OS_ERR         *)&err);
}

void BinaryHeap_init(void)
{	
	OS_ERR err;
	
	//printf ("Initialize Binary Heap\n");
	
	//Allocate Memory to Binary Heap Node
	OSMemCreate(
		(OS_MEM		*) &HeapNodeMEM,
		(CPU_CHAR	*) "Heap Node Partition",
		(void		*) &alloMemHeapNode,
		(OS_MEM_QTY  ) 20,
		(OS_MEM_SIZE ) sizeof(HeapNode),
		(OS_ERR		*) &err
	);
	
	//allocate memory to binary heap tree
	OSMemCreate(
		(OS_MEM		*) &HeapTreeMem,
		(CPU_CHAR	*) "Heap Tree Partition",
		(void		*) &alloMemHeapTree,
		(OS_MEM_QTY  ) 1,
		(OS_MEM_SIZE ) sizeof(HeapTree),
		(OS_ERR		*) &err
	);
	
	//initialize the node
	heapTree = (HeapTree*) OSMemGet((OS_MEM	*)&HeapTreeMem,
					(OS_ERR *)&err);
	
	heapTree->root = (HeapNode*)0;
	heapTree->data = &alloMemHeapNode[0];
	
	int i = 0;
	while (i < 20)
	{
		alloMemHeapNode[i].ptr_tcb = (OS_TCB*)0;
		i++;
	}
	
	heapTree->count = (CPU_INT08U) 0;
	heapTree->size = (CPU_INT08U )20;
}

/*
*****************************************************************************************************
                                   AVL TREE & FUNCTION PROTOTYPE
*****************************************************************************************************
*/
//Update time function calls the recursive update time
void AVLTree_Update_Time()
{
        temp_node *temp = roots;
        AVLTree_Recursive_Update(temp);
}

/*Recursively update the time remaining, once it reaches 0, 
    it will be inserted into binary heap.*/
void AVLTree_Recursive_Update(temp_node* curNode)
{       
        //If curNode = null, return
        if(curNode == (temp_node*)0)
            return;
        
        if(counter != 0)
        {
          if(counter%5 == 0) //Period = 5
                Insert_To_Binary_Heap(_search(curNode,5u));
          if(counter%15 == 0) //Period = 15
                Insert_To_Binary_Heap(_search(curNode,15u));
          if(counter%20 == 0) //Period = 20
                Insert_To_Binary_Heap(_search(curNode,20u));
          if(counter%35 == 0) //Period = 35
                Insert_To_Binary_Heap(_search(curNode,35u));
          if(counter%60 == 0) //Period = 60
                Insert_To_Binary_Heap(_search(curNode,60u));
        }
        
        //Increment Counter
        counter++;
}

void Insert_To_Binary_Heap(temp_node* curNode)
{
    OS_ERR err;
    Temp_TCB *tempo_tcb = (Temp_TCB*)0;
    tempo_tcb = curNode->data;
              
    OS_RecTaskBinaryHeapCreate(
         (OS_TCB        *) tempo_tcb->p_tcb,
         (CPU_CHAR      *) tempo_tcb->p_name, 
         (OS_TASK_PTR    ) tempo_tcb->p_task, 
         (void          *) tempo_tcb->p_arg, 
         (OS_PRIO        ) tempo_tcb->prio, 
         (CPU_STK       *) tempo_tcb->p_stk_base, 
         (CPU_STK_SIZE   ) tempo_tcb->stk_limit, 
         (CPU_STK_SIZE   ) tempo_tcb->stk_size, 
         (OS_MSG_QTY     ) tempo_tcb->q_size, 
         (OS_TICK        ) tempo_tcb->time_quanta, 
         (void          *) (CPU_INT32U) tempo_tcb->p_ext, 
         (OS_OPT         )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
         (OS_ERR        *) &err,
         (CPU_INT32U     ) tempo_tcb->period);
              
   Heap_Insert(tempo_tcb->p_tcb);
}

temp_node * _search(temp_node* T, int key)
{
	if (T == 0)
		return 0;

	if (key < T->data->period)
		return _search(T->left, key);
	else if (key > T->data->period)
		return _search(T->right, key);
	else
		return T;
}

temp_node *_insert(temp_node* T, Temp_TCB *x)
{
        OS_ERR      err;
        
	if (T == (temp_node*)0)
	{
		T = (temp_node*)OSMemGet((OS_MEM *)&AVL_MEM,
                                     (OS_ERR *)&err);
    
                T->data = (Temp_TCB*)OSMemGet((OS_MEM *)&OS_TCB_MEM,
                                         (OS_ERR *)&err);
		T->data = x;
		T->left = (temp_node*)0;
		T->right = (temp_node*)0;
	}
	else
	if (x->period > T->data->period)        /* insert in right subtree*/
	{
		T->right = _insert(T->right, x);
		if (_BF(T) == -2)
		if (x->period >T->right->data->period)
			T = _RR(T);
		else
			T = _RL(T);
	}
	else
	if (x->period < T->data->period)		/*insert in left subtree*/
	{
		T->left = _insert(T->left, x);
		if (_BF(T) == 2)
		if (x->period < T->left->data->period)
			T = _LL(T);
		else
			T = _LR(T);
	}

	T->ht = _height(T);
        
        return(T);
}

temp_node *_Delete(temp_node* T, int x)
{
	temp_node *p;

	if (T == 0)
	{
		return 0;
	}
	else if (x > T->data->period)        
	{
		T->right = _Delete(T->right, x);
		if (_BF(T) == 2)
		if (_BF(T->left) >= 0)
			T = _LL(T);
		else
			T = _LR(T);
	}
	else if (x < T->data->period)
	{
		T->left = _Delete(T->left, x);
		if (_BF(T) == -2)    /*Rebalance during windup*/
		if (_BF(T->right) <= 0)
			T = _RR(T);
		else
			T = _RL(T);
	}
	else
	{
		/*data to be deleted is found*/
		if (T->right != 0)
		{    /*delete its inorder succesor*/
			p = T->right;

			while (p->left != 0)
				p = p->left;

			T->data = p->data;
			T->right = _Delete(T->right, p->data->period);

			if (_BF(T) == 2)/*Rebalance during windup*/
			if (_BF(T->left) >= 0)
				T = _LL(T);
			else
				T = _LR(T);
		}
		else
			return(T->left);
	}
	T->ht = _height(T);
	return(T);
}

//Return the height of the tree
int _height(temp_node * T)
{
	int lh, rh;
	if (T == 0)
		return(0);

	if (T->left == 0)
		lh = 0;
	else
		lh = 1 + T->left->ht;

	if (T->right == 0)
		rh = 0;
	else
		rh = 1 + T->right->ht;

	if (lh>rh)
		return(lh);

	return(rh);
}

//Rotate right once
temp_node *_rotateright(temp_node * x)
{
	temp_node *y;
	y = x->left;
	x->left = y->right;
	y->right = x;
	x->ht = _height(x);
	y->ht = _height(y);
	return(y);
}

//Rotate left once
temp_node *_rotateleft(temp_node * x)
{
	temp_node *y;
	y = x->right;
	x->right = y->left;
	y->left = x;
	x->ht = _height(x);
	y->ht = _height(y);

	return(y);
}

temp_node * _RR(temp_node * T) /*Right-Right case*/
{
	T = _rotateleft(T);
	return(T);
}

temp_node * _LL(temp_node * T) /*Left-Left case*/
{
	T = _rotateright(T);
	return(T);
}

temp_node * _LR(temp_node * T) /*Left-Right case*/
{
	T->left = _rotateleft(T->left);
	T = _rotateright(T);

	return(T);
}

temp_node * _RL(temp_node * T) /*Right-Left case*/
{
	T->right = _rotateright(T->right);
	T = _rotateleft(T);
	return(T);
}

//Return the balancing factor of the node
int _BF(temp_node * T) /*Balance Factor = height of left sub-tree - height of right sub-tree*/
{
	int lh, rh; /*Left tree height and right tree height*/
	if (T == 0)
		return(0);

	if (T->left == 0) /*If the root got no left node, left tree height = 0*/
		lh = 0;
	else
		lh = 1 + T->left->ht;

	if (T->right == 0) /*If the root got no right node, right tree height = 0*/
		rh = 0;
	else
		rh = 1 + T->right->ht;

	return(lh - rh); /*Return the balance factor*/
}

void _inorder(temp_node *T) /*in-order traversal*/
{
	if (T != 0)
	{
		_inorder(T->left);
		printf("%d(Bf=%d)", T->data->period, _BF(T));
		_inorder(T->right);
	}
}

void print(temp_node *T)
{
	printf("\n\nInorder sequence:\n");
	inorder(T);
	printf("\n");
	printf("\nHeight: %d",height(roots));
	printf("\n");
        printf("\n");
}

/*
**********************************************************************************************************************************
                                            BINARY HEAP & FUNCTION PROTOTYPE
**********************************************************************************************************************************
*/
// Inserts element to the heap
void Heap_Insert(OS_TCB* _ptr_tcb)
{
      //start_time = OS_TS_GET();
  
      //get total number of the element in the list
      CPU_INT08U count = heapTree->count;
              
      //add the TCB into the data structure
      alloMemHeapNode[count].ptr_tcb = _ptr_tcb;
              
      //update the number of element
      count++;
              
      heapTree->count = count;
      //BinaryHeap_printTree();
      
      /*start_time2 = OS_TS_GET();
      insertion_time = start_time2 - start_time;
      insertion_time = insertion_time/50;
      overall_time = overall_time + insertion_time;
      printf("Insertion Time: %d us\n",insertion_time);*/
}

//Reordering the task according to the priority
void Heap_Heapify(void)
{
      //start_time = OS_TS_GET();
  
      //Gets current number of tasks
      CPU_INT08U count = heapTree->count;
      CPU_INT08U i = 0;
      HeapNode *lists  = &alloMemHeapNode[0];
      while(i < count)
      {
        Heap_heapify(i,lists);
        i++;
      }
      
      /*start_time2 = OS_TS_GET();
      heapifying_time = start_time2 - start_time;
      heapifying_time = heapifying_time/50;
      overall_time = overall_time + heapifying_time;
      printf("Heapifying Time: %d us\n",heapifying_time);*/
}

//PrintTree
void Heap_printTree(void)
{
      //Gets current number of tasks
      CPU_INT08U count = heapTree->count;
      CPU_INT08U i = 0;
      HeapNode *lists = heapTree->data;
      while(i<count)
      {
        printf("%d\n",lists[i].ptr_tcb->period);
        i++;
      }  
}

//Delete particular Nodes
void Heap_delete(OS_TCB *ptr_tcb)
{       
      //Get readyList
      HeapNode *lists = heapTree->data;
      //Search index of deleted Node
      CPU_INT08U deletedNode = Heap_search(ptr_tcb);
      //Gets current number of tasks
      CPU_INT08U count = heapTree->count;  
      
      if(deletedNode!= (CPU_INT08U)255)
      {
        Heap_swap(deletedNode,count-1);
      }
      
      lists[count-1].ptr_tcb = (OS_TCB*)0;
      Heap_Heapify();
      count--;
      heapTree->count = count;
}

//Pop the minimum node and heapify
OS_TCB* Heap_pop()
{
      OS_ERR err;
      //start_time = OS_TS_GET();
      
      //Get readyList
      HeapNode *lists = heapTree->data;
      OS_TCB* toBeReturned = lists[0].ptr_tcb;
      if(toBeReturned != 0)
      {
        Heap_delete(toBeReturned);
        
        OSMemPut( (OS_MEM *)&HeapTreeMem,
                          (void   *)lists,
                          (OS_ERR *) &err);
        
        /*start_time2 = OS_TS_GET();
        pop_min_time = start_time2 - start_time;
        pop_min_time = pop_min_time/50;
        overall_time = overall_time + pop_min_time;
        printf("Pop_min Time: %d us\n",pop_min_time);
        printf("Overall Time: %d us\n\n",overall_time);*/
        
        return toBeReturned;
      }
      else
      {
        return (OS_TCB*)0;
      }
}

//Return index of Nodes 
CPU_INT08U Heap_search(OS_TCB *ptr_tcb)
{
      //Gets current number of tasks
      CPU_INT08U count= heapTree->count;
      CPU_INT08U i = 0;
      HeapNode *lists = heapTree->data;
      while(i<count)
      {
        if(lists[i].ptr_tcb->period == ptr_tcb->period)
        {
          return i;
        }
        i++;
      }    
      return 255;
}

//Return LeftNodeIndex
CPU_INT08U Heap_leftNodeIndex(CPU_INT08U i)
{
    return i * 2 + 1;
}
  
//Return RightNodeIndex
CPU_INT08U Heap_rightNodeIndex(CPU_INT08U i)
{
    return i * 2 + 2;           
}

//Recursiviliy Heapify
void Heap_heapify(CPU_INT08U i,HeapNode *lists)
{
   if(i > 20)
      return;
   
   CPU_INT08U parent_Index = i;
   CPU_INT08U left_Index = Heap_leftNodeIndex(i);
   CPU_INT08U right_Index = Heap_rightNodeIndex(i);
   
   //if left node is valid
    if(left_Index < 20 && lists[left_Index].ptr_tcb != (OS_TCB *)0)
   {
     //Coniture go to the leftmost
     Heap_heapify(left_Index,lists);
     
     //If right is valid
      if(right_Index < 20 && lists[right_Index].ptr_tcb != (OS_TCB *)0)
     {
         if(lists[left_Index].ptr_tcb->period < lists[right_Index].ptr_tcb->period)
        {
           if(lists[left_Index].ptr_tcb->period < lists[parent_Index].ptr_tcb->period)
          {
            Heap_swap(left_Index,parent_Index);
          }
        }
        else
        {
           if(lists[right_Index].ptr_tcb->period < lists[parent_Index].ptr_tcb->period)
          {
            Heap_swap(right_Index,parent_Index);
          }         
        }
     }
     else
     {
       return;
     }
   }
   
   if(right_Index < 20)
   {
     Heap_heapify(right_Index,lists);
   }
}

//Swap the contents
void Heap_swap(CPU_INT08U i1,CPU_INT08U i2)
{
   OS_TCB *temp =  heapTree->data[i1].ptr_tcb;
   heapTree->data[i1].ptr_tcb =  heapTree->data[i2].ptr_tcb;
   heapTree->data[i2].ptr_tcb = temp;
}

/*
**************************************************************************************************************
                                              SCHEDULER TASK
**************************************************************************************************************
*/
void OS_RecTaskCreate(OS_TCB        *_p_tcb,
                      CPU_CHAR      *_p_name,
                      OS_TASK_PTR    _p_task,
                      void          *_p_arg,
                      OS_PRIO        _prio,
                      CPU_STK       *_p_stk_base,
                      CPU_STK_SIZE   _stk_limit,
                      CPU_STK_SIZE   _stk_size,
                      OS_MSG_QTY     _q_size,
                      OS_TICK        _time_quanta,
                      void          *_p_ext,
                      OS_OPT         _opt,
                      OS_ERR        *_p_err,
                      CPU_INT32U     _period,
                      CPU_INT32U     _time_remaining)
{
    OS_ERR err;
    
    Temp_TCB *temp_tcb = (Temp_TCB *)OSMemGet((OS_MEM*)&OS_Temp_MEM, (OS_ERR*)&err);
    
    temp_tcb->p_tcb             = _p_tcb;
    temp_tcb->p_name            = _p_name;
    temp_tcb->p_task            = _p_task;
    temp_tcb->p_arg             = _p_arg;
    temp_tcb->prio              = _prio;
    temp_tcb->p_stk_base        = _p_stk_base;
    temp_tcb->stk_limit         = _stk_limit;
    temp_tcb->stk_size          = _stk_size;
    temp_tcb->q_size            = _q_size;
    temp_tcb->time_quanta       = _time_quanta;
    temp_tcb->p_ext             = _p_ext;
    temp_tcb->opt               = _opt;
    temp_tcb->p_err             = _p_err;
    temp_tcb->time_remaining    = _time_remaining;
    temp_tcb->period            = _period;
    
    //Insert into AVL Tree
    roots = _insert(roots, temp_tcb);
}

void one_time_call(void)
{
        temp_node *temp = roots;
        one_time_insertion(temp);
}

void execute_task(void)
{
    OS_TCB *temp = Heap_pop();
    if(temp != 0)
    {
        OS_PrioInsert(temp->Prio);
        OS_RdyListInsertTail(temp);
    }
}

void one_time_insertion(temp_node* curNode)
{
    OS_ERR err;
    
    if(curNode == (temp_node*)0)
            return;
    
    Temp_TCB *tempo_tcb = (Temp_TCB*)0;
    tempo_tcb = curNode->data;
              
    OS_RecTaskBinaryHeapCreate(
                 (OS_TCB        *) tempo_tcb->p_tcb,
                 (CPU_CHAR      *) tempo_tcb->p_name, 
                 (OS_TASK_PTR    ) tempo_tcb->p_task, 
                 (void          *) tempo_tcb->p_arg, 
                 (OS_PRIO        ) tempo_tcb->prio, 
                 (CPU_STK       *) tempo_tcb->p_stk_base, 
                 (CPU_STK_SIZE   ) tempo_tcb->stk_limit, 
                 (CPU_STK_SIZE   ) tempo_tcb->stk_size, 
                 (OS_MSG_QTY     ) tempo_tcb->q_size, 
                 (OS_TICK        ) tempo_tcb->time_quanta, 
                 (void          *) (CPU_INT32U) tempo_tcb->p_ext, 
                 (OS_OPT         )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR        *) &err,
                 (CPU_INT32U     ) tempo_tcb->period);
              
   Heap_Insert(tempo_tcb->p_tcb);
   execute_task();
   
   //By in-order traversal
   one_time_insertion(curNode->left);
   one_time_insertion(curNode->right);
}

void basic(void)
{
    OS_ERR      err;
    CPU_TS      ts;
    OS_SEM_CTR  ctr;
    int count = 0;
    
    while(1)
    {
        //Waiting for Semaphore
        ctr = OSSemPend(&sema,0,OS_OPT_PEND_BLOCKING,&ts,&err);
        
        //Only run once
        if(count == 0)
        {
            //insert the 5 task into the Binary Heap at time 0
            one_time_call();
            count++;
        }
        
        //Update time remaining
        AVLTree_Update_Time();
    }
}

// Basic Task for while loop
void OS_Recstart(void)
{
    OS_ERR err;
    OSTaskCreate(
                 (OS_TCB     *)&BasicTaskTCB, 
                 (CPU_CHAR   *)"TaskScheduler", 
                 (OS_TASK_PTR ) basic, 
                 (void       *) 0, 
                 (OS_PRIO     ) 2u, 
                 (CPU_STK    *) &BasicTaskStk[0], 
                 (CPU_STK_SIZE) BasicTask_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) BasicTask_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *) (CPU_INT32U) 2, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err
    );   
}

/*
*********************************************************************************************************
                               CREATE RECURSIVE TASK FOR BINARY HEAP
*********************************************************************************************************
*/
void  OS_RecTaskBinaryHeapCreate (OS_TCB        *p_tcb,
                                  CPU_CHAR      *p_name,
                                  OS_TASK_PTR    p_task,
                                  void          *p_arg,
                                  OS_PRIO        prio,
                                  CPU_STK       *p_stk_base,
                                  CPU_STK_SIZE   stk_limit,
                                  CPU_STK_SIZE   stk_size,
                                  OS_MSG_QTY     q_size,
                                  OS_TICK        time_quanta,
                                  void          *p_ext,
                                  OS_OPT         opt,
                                  OS_ERR        *p_err,
                                  CPU_INT32U     period)
{
    CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
    if (prio >= OS_CFG_PRIO_MAX) {                          /* Priority must be within 0 and OS_CFG_PRIO_MAX-1        */
        *p_err = OS_ERR_PRIO_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
    
    p_tcb->period        = period;
    
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */

                                                            /* --------------- ADD TASK TO READY LIST --------------- */
    OS_CRITICAL_ENTER();
    
#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListAdd(p_tcb);
#endif

    OSTaskQty++;                                            /* Increment the #tasks counter                           */

    if (OSRunning != OS_STATE_OS_RUNNING) {                 /* Return if multitasking has not started                 */
        OS_CRITICAL_EXIT();
        return;
    }

    OS_CRITICAL_EXIT_NO_SCHED(); 
}
                  
                  